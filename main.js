
window.onload = function() {
    let btnMinus = document.querySelector("#btn_minus");
    let btnPlus = document.querySelector("#btn_plus");
    let btnReset = document.querySelector("#btn_reset");
    let counter = document.querySelector("#counter");
    let counterValue = document.querySelector("#counter").innerHTML*1;

    btnPlus.addEventListener("click", () => {
        (counterValue >= 999) ? counterValue = 999 : counterValue++;
        setCount();
    })

    btnMinus.addEventListener("click", () => {
        (counterValue <= 0) ? counterValue = 0: counterValue--;
        setCount();
    })

    btnReset.addEventListener("click", () => {
        counterValue = 0;
        setCount();
    })

    function setCount() {
        counter.innerHTML = counterValue;
    }
};